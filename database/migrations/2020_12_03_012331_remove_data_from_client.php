<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveDataFromClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('last_name');
            $table->dropColumn('web');
            $table->dropColumn('address');
            $table->dropColumn('location');
            $table->foreignId('country_id')->nullable()->onDelete('cascade')->after('phone');
            $table->string('worker_type');
            $table->string('worker_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn(['country_id']);
        });
    }
}
