<?php

namespace Database\Seeders;

use App\Imports\BranchesImport;
use App\Imports\ProductsImport;
use App\Models\Attribute;
use App\Models\Banner;
use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory()->create([
            'name' => 'Admin',
            'email' => 'admin@lafayette.com',
            'password' => Hash::make('abcYK-1233'),
            'is_admin' => true
        ]);

        (new BranchesImport)->import('database/seeders/files/branches.xlsx');

        $default_attributes = [
            'Contenido',
            'Categoría',
            'Uso',
            'Género'
        ];

        foreach ($default_attributes as $value) {
            Attribute::create([
                'name' => $value
            ]);
        }

        Banner::create([
            'url' => env('APP_URL') . '/img/banners/banner_default.jpg',
        ]);

        (new ProductsImport)->import('database/seeders/files/products.xlsx');

        $countries = 'Argentina,Brasil,México,Paraguay,Bolivia,Chile,Colombia,Costa Rica,República Dominicana,Ecuador,El Salvador,Guatemala,Honduras,Nicaragua,Panamá,Perú,Portugal,España,Estados Unidos,Canadá,Uruguay,Venezuela,Otro';
        $countries = explode(',', $countries);

        foreach ($countries as $country) {
            Country::create([
                'name' => $country
            ]);
        }
    }
}
