<?php

namespace App\Providers;

use App\Models\Attribute;
use App\Models\AttributeProduct;
use App\Models\Coupon;
use App\Observers\CouponObserver;
use App\Observers\SlugObserver;
use App\Observers\ValueSlugObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Coupon::observe(CouponObserver::class);
        Attribute::observe(SlugObserver::class);
        AttributeProduct::observe(ValueSlugObserver::class);
    }
}
