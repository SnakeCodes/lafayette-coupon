<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'document',
        'phone',
        'company',
        'city',
        'country_id',
        'country_text',
        'worker_type',
        'worker_name',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
