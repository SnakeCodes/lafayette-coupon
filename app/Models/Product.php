<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'gender',
        'image',
        'file',
        'highlight'
    ];

    protected $casts = [
        'highlight' => 'boolean'
    ];

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class)->using(AttributeProduct::class)->withPivot(['value', 'value_slug']);
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function ($builder) {
            $builder->orderBy('id', 'DESC');
        });
    }

    public function campaigns()
    {
        return $this->belongsToMany(Campaign::class);
    }
}
