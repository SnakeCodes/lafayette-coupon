<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'enable',
    ];

    protected $casts = [
        'enable' => 'boolean'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
