<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CampaignProduct extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'product_id',
    ];

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }
}
