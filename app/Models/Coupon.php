<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'client_id',
        'branch_id',
        'product_id',
        'campaign_id',
        'adviser',
        'enable',
        'created_by_id'
    ];

    protected $casts = [
        'enable' => 'boolean'
    ];

    public function created_by()
    {
        return $this->belongsTo(User::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function download()
    {
        return $this->hasOne(Download::class);
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }
}
