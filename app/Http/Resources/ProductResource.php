<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'gender' => $this->gender,
            'highlight' => $this->highlight,
            'description' => $this->description,
            'file' => $this->file,
            'attributes' => $this->attributes,
            'campaigns' => $this->campaigns
        ];
    }
}
