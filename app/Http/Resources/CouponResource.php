<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CouponResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'enable' => $this->enable,
            'branch' => new BranchResource($this->branch),
            'client' => new ClientResource($this->client),
            'campaign' => new CampaignResource($this->campaign)
        ];
    }
}
