<?php

namespace App\Http\Controllers\Api;

use App\Exports\ProductExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Coupon;
use App\Models\Download;
use App\Models\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::when($request->has('attributes'), function ($query) use ($request) {
            foreach ($request->get('attributes') as $key => $value) {
                $query->whereHas('attributes', function ($query) use ($request, $key, $value) {
                    $query->where(function ($query) use ($request, $key, $value) {
                        $query->where('attribute_id', $key)->where(function ($query) use ($value) {
                            foreach ($value['value'] as $key => $slug) {
                                $query->where('value_slug', $slug);
                            }
                        });
                    });
                });
            }
        })->when($request->has('query'), function ($query) use ($request) {
            $query->where('name', 'like', "%{$request->get('query')}%");
        })->orderBy('highlight', 'desc')->get();

        return ProductResource::collection($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $data = $request->all();

        if ($request->has('file') && $request->file) {
            $img = $request->file('file');
            $ext = $img->getClientOriginalExtension();
            $name = time() . '.' . $ext;
            $path = public_path('/files/products');
            $img->move($path, $name);
            $data['file'] = $name;
        }

        if ($request->has('image') && $request->image) {
            $img = $request->file('image');
            $ext = $img->getClientOriginalExtension();
            $name = time() . '.' . $ext;
            $path = public_path('/img/products');
            $img->move($path, $name);
            $data['image'] = env('APP_URL') . '/img/products/' . $name;
        }

        $data['highlight'] = filter_var($request->highlight, FILTER_VALIDATE_BOOLEAN);

        $product = Product::create($data);
        $product->attributes()->sync($request->get('attributes'));
        return new ProductResource($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $data = $request->all();
        $data['highlight'] = filter_var($request->highlight, FILTER_VALIDATE_BOOLEAN);
        $product->update($data);
        if ($request->has('attributes')) {
            $product->attributes()->sync($request->get('attributes'));
        }
        return new ProductResource($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json(null);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function download(Request $request, Product $product)
    {
        if (!$request->has('coupon')) {
            echo "Necesitas un cupón";
            exit();
        }

        if ($request->has('coupon')) {

            $coupon = Coupon::where('code', $request->get('coupon'))->first();

            if (!$coupon) {
                echo "El cupón no existe";
                exit();
            }

            if (!$coupon->enable) {
                echo "Tu cupón ya fue usado";
                exit();
            }

            if ($coupon->campaign != null) {
                if (!$coupon->campaign->products->contains($product->id)) {
                    echo "Tu cupón no puede ser usado para esta campaña";
                }
            }

            try {

                $file = public_path() . '/files/products/' . $product->file;

                $filetype = filetype($file);

                $filename = basename($file);

                header("Content-Type: " . $filetype);

                header("Content-Length: " . filesize($file));

                header("Content-Disposition: attachment; filename=" . $filename);

                readfile($file);

                Download::create([
                    'coupon_id' => $coupon->id,
                    'product_id' => $product->id,
                    'client_id' => $coupon->client->id,
                ]);

                $coupon->enable = false;
                $coupon->save();
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }

    public function export()
    {
        return Excel::download(new ProductExport, 'products.xlsx');
    }
}
