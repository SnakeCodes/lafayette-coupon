<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AttributeProductResource;
use App\Models\AttributeProduct;

class AttributeProductController extends Controller
{

    public function __invoke()
    {
        return AttributeProductResource::collection(AttributeProduct::select(['id', 'attribute_id', 'value', 'value_slug'])->get());
    }
}
