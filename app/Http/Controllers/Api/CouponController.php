<?php

namespace App\Http\Controllers\Api;

use App\Exports\CouponExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreClientCoupon;
use App\Http\Requests\StoreCoupon;
use App\Http\Resources\CouponResource;
use App\Models\Client;
use App\Models\Coupon;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return CouponResource::collection(Coupon::when($request->has('date1') && $request->has('date2'), function ($query) use ($request) {
            $date1 = new Carbon($request->get('date1'));
            $date2 = new Carbon($request->get('date2'));
            $query->whereBetween('created_at', [$date1->startOfDay(), $date2->endOfDay()]);
        })->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCoupon $request)
    {
        return new CouponResource(Coupon::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        return new CouponResource($coupon);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        $coupon->update($request->all());
        return new CouponResource($coupon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        $coupon->delete();
        return response()->json(null);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generate(StoreClientCoupon $request)
    {
        $client = Client::firstOrCreate($request->client);
        if (!$client) {
            return response()->json(['errors' => ['No se pudo crear el cliente']], 422);
        }
        $request['client_id'] = $client->id;
        $request['created_by_id'] = Auth::id();

        $coupon = Coupon::create($request->all());
        if (!$coupon) {
            return response()->json(['errors' => ['No se pudo crear el cupon']], 422);
        }

        return new CouponResource($coupon);
    }


    public function check(Request $request)
    {
        $coupon = Coupon::where('code', $request->get('coupon'))->first();

        if (!$coupon) {
            return response()->json(['errors' => ['El cupón no existe.']], 422);
        }

        if (!$coupon->enable) {
            return response()->json(['errors' => ['El cupón ya fue usado.']], 422);
        }

        if ($coupon->campaign != null && $request->has('product_id')) {
            if (!$coupon->campaign->products->contains($request->get('product_id'))) {
                return response()->json(['errors' => ['Tu cupón no puede ser usado para esta campaña. Consulta con tu tienda o asesor comercial.']], 422);
            }
        }

        return new CouponResource($coupon);
    }

    public function export()
    {
        return Excel::download(new CouponExport, 'coupons.xlsx');
    }
}
