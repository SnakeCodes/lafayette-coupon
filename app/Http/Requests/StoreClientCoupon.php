<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreClientCoupon extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'branch_id' => 'required|integer|exists:branches,id',
            'client.name' => 'required|string',
            'client.document' => 'required|string',
            'client.email' => 'required|email',
            'client.phone' => 'required',
            'client.country_id' => 'required',
            'client.city' => 'required',
        ];
    }
}
