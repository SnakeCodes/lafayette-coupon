<?php

namespace App\Exports;

use App\Models\Client;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ClientExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    public function headings(): array
    {
        return [
            '#',
            'Nombre',
            'Documento',
            'Email',
            'Telefono',
            'Compañia',
            'Pais',
            'Ciudad',
            'Fecha de creación',
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Client::all();
    }

    /**
     * @var Client $client
     */
    public function map($client): array
    {
        return [
            $client->id,
            $client->name,
            $client->document,
            $client->email,
            $client->phone,
            $client->company,
            $client->country->name,
            $client->city,
            $client->created_at,
        ];
    }
}
