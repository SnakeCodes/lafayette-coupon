<?php

namespace App\Exports;

use App\Models\Attribute;
use App\Models\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProductExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    public function headings(): array
    {
        $default = [
            '#',
            'Nombre',
            'Imagen',
            'Archivo',
        ];

        $end = ['Descripción', 'Fecha de creación'];

        $data = array_merge($default, Attribute::all()->pluck('name')->toArray(), $end);

        return $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Product::all();
    }

    /**
     * @var Product $product
     */
    public function map($product): array
    {
        $attributes = Attribute::all();
        $pa = array();

        foreach ($attributes as $attribute) {
            $result = $product->attributes->first(function ($value, $key) use ($attribute) {
                return $value->id == $attribute->id;
            });

            array_push($pa, ($result) ? $result->pivot->value : '');
        }

        $default = [
            $product->id,
            $product->name,
            $product->image,
            $product->file
        ];

        $end = [
            $product->description,
            $product->created_at
        ];

        $data = array_merge($default, $pa, $end);

        return  $data;
    }
}
