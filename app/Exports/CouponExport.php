<?php

namespace App\Exports;

use App\Models\Attribute;
use App\Models\Coupon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CouponExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    public function headings(): array
    {
        $array1 = [
            '#',
            'Cliente - Nombre',
            'Cliente - Documento',
            'Cliente - Email',
            'Cliente - Telefono',
            'Cliente - Compañia',
            'Cliente - Pais',
            'Cliente - Ciudad',
        ];

        $array2 = [
            'Campaña',
            'Codigo',
            'Codigo usado?',
            'Tienda',
            'Asesor',
            'Cupon creado por',
            'Fecha de creación',
        ];

        $product = [
            'Producto - Nombre',
            'Producto - Imagen',
            'Producto - Archivo',
        ];

        $map = Attribute::all()->pluck('name')->map(function ($val) {
            return 'Producto - ' . $val;
        })->toArray();


        return array_merge($array1, $product, $map, $array2);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Coupon::all();
    }

    /**
     * @var Coupon $coupon
     */
    public function map($coupon): array
    {
        $attributes = Attribute::all();
        $pa = array();

        if ($coupon->download && !$coupon->enable) {
            foreach ($attributes as $attribute) {
                $result = $coupon->download->product->attributes->first(function ($value) use ($attribute) {
                    return $value->id == $attribute->id;
                });

                array_push($pa, ($result) ? $result->pivot->value : 'None');
            }
        } else {
            foreach ($attributes as $attribute) {
                array_push($pa, ' ');
            }
        }


        $array1 = [
            $coupon->id,
            $coupon->client->name,
            $coupon->client->document,
            $coupon->client->email,
            $coupon->client->phone,
            $coupon->client->company,
            $coupon->client->country_text ? $coupon->client->country : $coupon->client->country->name,
            $coupon->client->city,
            $coupon->download ? $coupon->download->product->name : ' ',
            $coupon->download ? $coupon->download->product->image : ' ',
            $coupon->download ? $coupon->download->product->file : ' ',
        ];

        $array2 = [
            $coupon->campaign ? $coupon->campaign->name : 'No',
            $coupon->code,
            ($coupon->enable ? 'Sin usar' : 'Usado'),
            $coupon->branch->name,
            $coupon->adviser ?? ' ',
            $coupon->created_by->name,
            $coupon->created_at,
        ];

        return array_merge($array1, $pa, $array2);
    }
}
