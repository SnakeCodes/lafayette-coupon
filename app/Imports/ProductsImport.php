<?php

namespace App\Imports;

use App\Models\Attribute;
use App\Models\Product;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class ProductsImport implements ToCollection, WithHeadingRow
{
    use Importable;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection  $rows)
    {
        $content_attribute = Attribute::where('slug', Str::slug('Contenido'))->first();
        $category_attribute = Attribute::where('slug', Str::slug('Categoría'))->first();
        $use_attribute = Attribute::where('slug', Str::slug('Uso'))->first();
        $gender_attribute = Attribute::where('slug', Str::slug('Genero'))->first();

        foreach ($rows as $row) {
            $product = Product::create([
                'name' => ucwords(mb_strtolower($row['contenido'])) . ' ' . ucwords(mb_strtolower($row['categoria'])) . ' ' . ucwords(mb_strtolower($row['genero'])),
                'image' => env('APP_URL') . '/img/products/' . $row['imagen'],
                'file' => $row['archivo'],
                'description' => $row['descripcion']
            ]);

            $product->attributes()->sync([
                $content_attribute->id => ['value' => $row['contenido']],
                $category_attribute->id => ['value' => $row['categoria']],
                $use_attribute->id => ['value' => $row['uso']],
                $gender_attribute->id => ['value' => $row['genero']],
            ]);
        }
    }
}
