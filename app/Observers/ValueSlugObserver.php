<?php

namespace App\Observers;

use Illuminate\Support\Str;

class ValueSlugObserver
{
    /**
     * Handle the "creating" event.
     *
     * @param  \App\Models\*  $model
     * @return void
     */
    public function creating($model)
    {
        $model->value_slug = Str::slug($model->value, '-');
    }

    /**
     * Handle the "updating" event.
     *
     * @param  \App\Models\* $model
     * @return void
     */
    public function updating($model)
    {
        if ($model->isDirty('value')) {
            $model->value_slug = Str::slug($model->value, '-');
        }
    }
}
