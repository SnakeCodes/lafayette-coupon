<?php

namespace App\Observers;

use Illuminate\Support\Str;

class SlugObserver
{
    /**
     * Handle the "creating" event.
     *
     * @param  \App\Models\*  $model
     * @return void
     */
    public function creating($model)
    {
        $model->slug = Str::slug($model->name, '-');
    }

    /**
     * Handle the "updating" event.
     *
     * @param  \App\Models\* $model
     * @return void
     */
    public function updating($model)
    {
        if ($model->isDirty('name')) {
            $model->slug = Str::slug($model->name, '-');
        }
    }
}
