<?php

namespace App\Observers;

use App\Mail\CouponGenerated;
use App\Models\Coupon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class CouponObserver
{
    /**
     * Handle the Coupon "creating" event.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return void
     */
    public function creating(Coupon $coupon)
    {
        do {
            if ($coupon->campaign != null) {
                $code = Str::slug($coupon->campaign->name, '-') . '-' . Str::random(5);
            } else {
                $code = $coupon->branch->code . '-' . Str::random(5);
            }
            $coupon->code = strtoupper($code);
        } while (Coupon::where('code', $code)->first());
    }

    /**
     * Handle the Coupon "created" event.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return void
     */
    public function created(Coupon $coupon)
    {
        Mail::to($coupon->client->email)->cc(env('MAIL_FROM_ADDRESS', 'servicioalcliente@lafayette.com'))->send(new CouponGenerated($coupon));
    }

    /**
     * Handle the Coupon "updated" event.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return void
     */
    public function updated(Coupon $coupon)
    {
    }

    /**
     * Handle the Coupon "deleted" event.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return void
     */
    public function deleted(Coupon $coupon)
    {
        //
    }

    /**
     * Handle the Coupon "restored" event.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return void
     */
    public function restored(Coupon $coupon)
    {
        //
    }

    /**
     * Handle the Coupon "force deleted" event.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return void
     */
    public function forceDeleted(Coupon $coupon)
    {
        //
    }
}
