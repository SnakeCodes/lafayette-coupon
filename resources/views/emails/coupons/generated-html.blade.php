<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0" />
    <style type="text/css">
        v\:* {
            behavior: url(#default#VML);
            display: inline-block;
        }
    </style>
    <style>
        a {
            text-decoration: none;
        }

        .column {
            min-width: 0px !important;
        }

        .innerMultiTD,
        .innerTD {
            height: 1px;
        }

        /* ESTILOS PARA OUTLOOK */
        body {
            padding: 0 !important;
            margin: 0 !important;
        }

        body,
        table,
        td,
        a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        /* iOS LINKS AZULES HEREDAN COLORES DEL HTML */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        @media only screen and (max-width: 599px) {
            .mobileColumn {
                width: 100% !important;
                max-width: 100% !important;
            }

            .tdMobile {
                margin: 0px !important;
                padding: 0px !important;
            }

            img {
                max-width: 100% !important;
                width: 100% !important;
            }

            #newsTable {
                margin: 0;
                padding: 0;
                width: 100% !important;
            }
        }
    </style>
</head>

<body style="text-align: center;min-width:100%;">
    <div align="center" style="margin-left:auto;margin-right:auto;">
        <table align='center' border='0' cellpadding='0' cellspacing='0' bgcolor='#ffffff' width='100%' id='backgroundTable'>
            <tbody>
                <tr>
                    <td class='wrap' align='center' valign='top' width='100%'>
                        <table align='center' border='0' cellpadding='0' cellspacing='0' width='600' id='newsTable' style='width:600px;'>
                            <tbody>
                                <tr>
                                    <td class='wrap' align='center' valign='top' width='100%'>
                                        <center>
                                            <!-- content -->
                                            <div id='container' style='padding: 0px;'>
                                                <div align='center' style='z-index: 999;'>
                                                    <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                        <tbody>
                                                            <tr>
                                                                <td valign='top' style='padding: 0px;'>
                                                                    <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td valign='top' style='padding: 0px;'>
                                                                                    <table cellpadding='0' cellspacing='0' width='100%'>
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <!-- Box Content -->
                                                                                                <td style='padding: 0px; width: 100%' class='innerTD'>
                                                                                                    <div align='center'>
                                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td align='center' style='padding: 0px;'>
                                                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='auto' align='center' style='z-index: 999;'>
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td valign='top' align='center' style='padding: 0px;'>
                                                                                                                                        <table cellpadding='0' cellspacing='0' width='100%' align='center'>
                                                                                                                                            <tbody>
                                                                                                                                                <tr style=''>
                                                                                                                                                    <!-- Image Content -->
                                                                                                                                                    <td style='padding: 0px;'>
                                                                                                                                                        <img src='https://mcusercontent.com/4ef046a39080ea7b4a1499c56/images/141b7212-2267-4a09-904e-d390d8c4d7a7.jpg' width='600' alt='' border='0' style='display: block; width: 600px; height: auto; max-width: 600px; position: relative; visibility: visible;'>
                                                                                                                                                    </td>
                                                                                                                                                    <!-- End Image Content -->
                                                                                                                                                </tr>
                                                                                                                                            </tbody>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </td>
                                                                                                <!-- End Box Content -->
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>



                                                </div>
                                                <div style='width: auto; margin: 0 auto; max-width: 100%; z-index: 999;' align='center'>
                                                    <!--[if (gte mso 9)|(IE)]><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td align='center'><![endif]-->
                                                    <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                        <tbody>
                                                            <tr>
                                                                <td valign='top' style='padding: 0px; min-width: 20px;' class='column' width='3.3333333333333335%'>
                                                                    <table align='left' cellpadding='0' cellspacing='0' width='100%' style='float: left;'>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td valign='top' style='padding: 0px; display: table-cell;' class='innerMultiTD'><img class='imgPlaceholder' src='https://s3.amazonaws.com/imagesrepository.icommarketing.com/placeholder.gif' width='20' height='2' style='display: block; width: 20px; overflow: hidden;'></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td valign='top' style='padding: 0px; min-width: 560px;' class='column' width='93.33333333333333%'>
                                                                    <table align='left' cellpadding='0' cellspacing='0' width='100%' style='float: left;'>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td valign='top' style='padding: 0px; display: table-cell;' class='innerMultiTD'>
                                                                                    <div align='center'>
                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td valign='top' align='center' style='padding: 0px;'>
                                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <!-- Text Content -->
                                                                                                                    <td valign='top' style='font-family: Arial; line-height: 1.2;'>
                                                                                                                        <p style='margin-top: 0px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden; text-align: left;'><br></p>
                                                                                                                        <p style='margin-top: 0px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden; text-align: left;'><br></p>
                                                                                                                        <p style='margin-top: 0px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden; text-align: left;'><br></p>
                                                                                                                        <p style='margin-top: 0px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden; text-align: left;'><span style='font-family:Helvetica,Arial,Verdana;'><span style='font-size:18px;'>¡Hola, un gusto en saludarte! </span></span></p>

                                                                                                                        <p style='margin-top: 15px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden; text-align: left;'><span style='font-family:Helvetica,Arial,Verdana;'><span style='font-size:18px;'>Se ha generado un cupón para que lo uses en nuestra página, te recordamos que es valido para <b>una sola descarga</b> @if ($coupon->campaign == null && !$coupon->campaign()->exists()) en cualquiera de nuestros contenidos. @endif</span></span></p>
                                                                                                                        @if ($coupon->campaign != null && $coupon->campaign()->exists())
                                                                                                                        <p style='margin-top: 15px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden; text-align: left;'><span style='font-family:Helvetica,Arial,Verdana;'><span style='font-size:18px;'>Puedes usar este cupón en los contenidos marcados exclusivamente con la <b>campaña: {{ $coupon->campaign->name }}</b>, solo debes copiar y pegar el código del cupón en el contenido que quieras descargar.</span></span></p>
                                                                                                                        @endif
                                                                                                                        <p style='margin-top: 0px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden; text-align: left;'><br></p>
                                                                                                                        <p style='margin-top: 0px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden; text-align: left;'><br></p>
                                                                                                                    </td><!-- End Text Content -->
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div align='center'>
                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style='padding: 0px;'>
                                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='auto' align='center' style='z-index: 999;'>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <!-- Button Content -->
                                                                                                                    <td valign='top' align='center' style='padding: 0px; position: relative; display:block'>
                                                                                                                        <div style='border-width: 2px; border-top-style: none; border-bottom-style: solid; border-left-style: none; border-color: rgb(150, 150, 150); border-image: initial; border-radius: 0; background: rgb(222, 0, 33); width: auto; color: black;'>
                                                                                                                            <div class='element-inner content-element' style='padding: 10px;'>
                                                                                                                                <p style='margin-top: 0px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden;'><span style='font-family:Helvetica,Arial,Verdana;'><strong><span style='color:#ffffff;'>Tu cupón es: {{$coupon->code}}</strong></span></p>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </td>
                                                                                                                    <!-- End Button Content -->
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='auto' align='center' style='z-index: 999; margin-top: 10px'>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <!-- Button Content -->
                                                                                                                    <td valign='top' align='center' style='padding: 0px; position: relative;'>
                                                                                                                        <div style='border-width: 2px; border-top-style: none; border-bottom-style: solid; border-left-style: none; border-color: rgb(150, 150, 150); border-image: initial; border-radius: 10px; background: rgb(222, 0, 33); width: auto; color: black;'>
                                                                                                                            <div class='element-inner content-element' style='padding: 10px;'>
                                                                                                                                <p style='margin-top: 0px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden;'><span style='font-family:Helvetica,Arial,Verdana;'><strong><span style='color:#ffffff;'><a href="{{ env('WEB_URL') }}" style='color: #ffffff;'>IR A LA PÁGINA</a></span></strong></span></p>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </td>
                                                                                                                    <!-- End Button Content -->
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div align='center'>
                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td valign='top' align='center' style='padding: 0px;'>
                                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <!-- Text Content -->
                                                                                                                    <h2 style='margin-top: 0px; margin-bottom: 0px; text-align: center; overflow-wrap: break-word; overflow: hidden;'><br></h2>
                                                                                                                    <h2 style='margin-top: 0px; margin-bottom: 0px; text-align: center; overflow-wrap: break-word; overflow: hidden;'><strong><span style='font-size:20px;'><span style='font-family:Helvetica,Arial,Verdana;'>¡Gracias por visitarnos!</span></span></strong></h2>
                                                                                                                    <p style='margin-top: 0px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden; text-align: left;'><br></p>
                                                                                                                    <p style='margin-top: 0px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden; text-align: left;'><span style='font-family:Helvetica,Arial,Verdana;'><span style='font-size:18px;'>Saludos</span></span></p>
                                                                                                                    <p style='margin-top: 0px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden; text-align: left;'><strong><span style='font-family:Helvetica,Arial,Verdana;'><span style='font-size:18px;'>Equipo Lafayette Sports</span></span></strong></p>
                                                                                                                    <p style='margin-top: 0px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden; text-align: left;'><br></p>
                                                                                                                    <p style='margin-top: 0px; margin-bottom: 0px; overflow-wrap: break-word; overflow: hidden; text-align: left;'><br></p><!-- End Text Content -->
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td valign='top' style='padding: 0px; min-width: 20px;' class='column' width='3.3333333333333335%'>
                                                                    <table align='left' cellpadding='0' cellspacing='0' width='100%' style='float: left;'>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td valign='top' style='padding: 0px; display: table-cell;' class='innerMultiTD'><img class='imgPlaceholder' src='https://s3.amazonaws.com/imagesrepository.icommarketing.com/placeholder.gif' width='20' height='2' style='display: block; width: 20px; overflow: hidden;'></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                                </div>
                                                <div align='center' style='z-index: 999;'>
                                                    <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                        <tbody>
                                                            <tr>
                                                                <td valign='top' style='padding: 0px;'>
                                                                    <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td valign='top' style='padding: 0px; background: rgb(103, 103, 103);'>
                                                                                    <table cellpadding='0' cellspacing='0' width='100%'>
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <!-- Box Content -->
                                                                                                <td style='padding: 0px; width: 100%' class='innerTD'>
                                                                                                    <div align='center'>
                                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td valign='top' align='center' style='padding: 15px 0px 5px;'>
                                                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <!-- Text Content -->
                                                                                                                                    <td valign='top' style='font-family: Arial; line-height: 1.2;'>
                                                                                                                                        <p style='margin-top: 0px; margin-bottom: 0px; text-align: center; overflow-wrap: break-word; overflow: hidden;'><em><span style='color:#ffffff;'><span style='line-height:normal'><span lang='ES' style='font-size:10.0pt'><span style='font-family:&quot;Helvetica LT Std Cond&quot;,sans-serif'>¡Siguenos en nuestras redes y aprende más con nuestros contenidos! </span></span></span>​​​​​​​</span></em></p>
                                                                                                                                    </td><!-- End Text Content -->
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                    <div style='width: auto; margin: 0 auto; max-width: 100%; z-index: 999;' align='center'>
                                                                                                        <!--[if (gte mso 9)|(IE)]><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td align='center'><![endif]-->
                                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td valign='top' style='padding: 0px; min-width: 200px;' class='column' width='33.333333333333336%'>
                                                                                                                        <table align='left' cellpadding='0' cellspacing='0' width='100%' style='float: left;'>
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td valign='top' style='padding: 0px; display: table-cell;' class='innerMultiTD'><img class='imgPlaceholder' src='https://s3.amazonaws.com/imagesrepository.icommarketing.com/placeholder.gif' width='200' height='2' style='display: block; width: 200px; overflow: hidden;'></td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                    <td valign='top' style='padding: 0px; min-width: 50px;' class='column' width='8.333333333333334%'>
                                                                                                                        <table align='left' cellpadding='0' cellspacing='0' width='100%' style='float: left;'>
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td valign='top' style='padding: 0px; display: table-cell;' class='innerMultiTD'>
                                                                                                                                        <div align='center'>
                                                                                                                                            <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                                                                <tbody>
                                                                                                                                                    <tr>
                                                                                                                                                        <td align='center' style='padding: 0px;'>
                                                                                                                                                            <table cellpadding='0' cellspacing='0' border='0' width='auto' align='center' style='z-index: 999;'>
                                                                                                                                                                <tbody>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td valign='top' align='center' style='padding: 0px;'>
                                                                                                                                                                            <table cellpadding='0' cellspacing='0' width='100%' align='center'>
                                                                                                                                                                                <tbody>
                                                                                                                                                                                    <tr>
                                                                                                                                                                                        <!-- Image Content -->
                                                                                                                                                                                        <td style='padding: 8px;'>
                                                                                                                                                                                            <a href='https://www.facebook.com/LafayetteSports/' target='_blank' data-label='Facebook Lafayette Sports'><img src='https://s3.amazonaws.com/imagesrepository.icommarketing.com/ImagesRepo/ODQ2LTE4ODQtbGFmYXlldHRlY281/1894/Facebook-bl.png' width='34' alt='Facebook Lafayette Sports' border='0' style='display: block; width: 34px; height: auto; max-width: 34px; position: relative; visibility: visible;' imgsrc='' imgalt='Facebook Lafayette Sports'></a>
                                                                                                                                                                                        </td>
                                                                                                                                                                                        <!-- End Image Content -->
                                                                                                                                                                                    </tr>
                                                                                                                                                                                </tbody>
                                                                                                                                                                            </table>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </tbody>
                                                                                                                                                            </table>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                        </div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                    <td valign='top' style='padding: 0px; min-width: 50px;' class='column' width='8.333333333333334%'>
                                                                                                                        <table align='left' cellpadding='0' cellspacing='0' width='100%' style='float: left;'>
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td valign='top' style='padding: 0px; display: table-cell;' class='innerMultiTD'>
                                                                                                                                        <div align='center'>
                                                                                                                                            <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                                                                <tbody>
                                                                                                                                                    <tr>
                                                                                                                                                        <td align='center' style='padding: 0px;'>
                                                                                                                                                            <table cellpadding='0' cellspacing='0' border='0' width='auto' align='center' style='z-index: 999;'>
                                                                                                                                                                <tbody>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td valign='top' align='center' style='padding: 0px;'>
                                                                                                                                                                            <table cellpadding='0' cellspacing='0' width='100%' align='center'>
                                                                                                                                                                                <tbody>
                                                                                                                                                                                    <tr>
                                                                                                                                                                                        <!-- Image Content -->
                                                                                                                                                                                        <td style='padding: 8px;'>
                                                                                                                                                                                            <a href='https://www.instagram.com/lafayette_sports' target='_blank' data-label='Instagram Lafayette Sports'><img src='https://s3.amazonaws.com/imagesrepository.icommarketing.com/ImagesRepo/ODQ2LTE4ODQtbGFmYXlldHRlY281/1894/Instagram-bl.png' width='34' alt='Instagram Lafayette Sports' border='0' style='display: block; width: 34px; height: auto; max-width: 34px; position: relative; visibility: visible;' imgsrc='' imgalt='Instagram Lafayette Sports'></a>
                                                                                                                                                                                        </td>
                                                                                                                                                                                        <!-- End Image Content -->
                                                                                                                                                                                    </tr>
                                                                                                                                                                                </tbody>
                                                                                                                                                                            </table>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </tbody>
                                                                                                                                                            </table>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                        </div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                    <td valign='top' style='padding: 0px; min-width: 50px;' class='column' width='8.333333333333334%'>
                                                                                                                        <table align='left' cellpadding='0' cellspacing='0' width='100%' style='float: left;'>
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td valign='top' style='padding: 0px; display: table-cell;' class='innerMultiTD'>
                                                                                                                                        <div align='center'>
                                                                                                                                            <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                                                                <tbody>
                                                                                                                                                    <tr>
                                                                                                                                                        <td align='center' style='padding: 0px;'>
                                                                                                                                                            <table cellpadding='0' cellspacing='0' border='0' width='auto' align='center' style='z-index: 999;'>
                                                                                                                                                                <tbody>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td valign='top' align='center' style='padding: 0px;'>
                                                                                                                                                                            <table cellpadding='0' cellspacing='0' width='100%' align='center'>
                                                                                                                                                                                <tbody>
                                                                                                                                                                                    <tr>
                                                                                                                                                                                        <!-- Image Content -->
                                                                                                                                                                                        <td style='padding: 8px;'>
                                                                                                                                                                                            <a href='https://www.pinterest.es/lafayette_sports/boards/' target='_blank' data-label='Pinteres Lafayette Sports'><img src='https://s3.amazonaws.com/imagesrepository.icommarketing.com/ImagesRepo/ODQ2LTE4ODQtbGFmYXlldHRlY281/1894/pinteres-bl.png' width='34' alt='Pinteres Lafayette Sports' border='0' style='display: block; width: 34px; height: auto; max-width: 34px; position: relative; visibility: visible;' imgsrc='' imgalt='Pinteres Lafayette Sports'></a>
                                                                                                                                                                                        </td>
                                                                                                                                                                                        <!-- End Image Content -->
                                                                                                                                                                                    </tr>
                                                                                                                                                                                </tbody>
                                                                                                                                                                            </table>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </tbody>
                                                                                                                                                            </table>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                        </div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                    <td valign='top' style='padding: 0px; min-width: 50px;' class='column' width='8.333333333333334%'>
                                                                                                                        <table align='left' cellpadding='0' cellspacing='0' width='100%' style='float: left;'>
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td valign='top' style='padding: 0px; display: table-cell;' class='innerMultiTD'>
                                                                                                                                        <div align='center'>
                                                                                                                                            <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                                                                <tbody>
                                                                                                                                                    <tr>
                                                                                                                                                        <td align='center' style='padding: 0px;'>
                                                                                                                                                            <table cellpadding='0' cellspacing='0' border='0' width='auto' align='center' style='z-index: 999;'>
                                                                                                                                                                <tbody>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td valign='top' align='center' style='padding: 0px;'>
                                                                                                                                                                            <table cellpadding='0' cellspacing='0' width='100%' align='center'>
                                                                                                                                                                                <tbody>
                                                                                                                                                                                    <tr>
                                                                                                                                                                                        <!-- Image Content -->
                                                                                                                                                                                        <td style='padding: 8px;'>
                                                                                                                                                                                            <a href='https://www.linkedin.com/showcase/lafayette-sports' target='_blank' data-label='Linkedin Lafayette Sports'><img src='https://s3.amazonaws.com/imagesrepository.icommarketing.com/ImagesRepo/ODQ2LTE4ODQtbGFmYXlldHRlY281/1894/linkedIn-bl.png' width='34' alt='Linkedin Lafayette Sports' border='0' style='display: block; width: 34px; height: auto; max-width: 34px; position: relative; visibility: visible;' imgsrc='' imgalt='Linkedin Lafayette Sports'></a>
                                                                                                                                                                                        </td>
                                                                                                                                                                                        <!-- End Image Content -->
                                                                                                                                                                                    </tr>
                                                                                                                                                                                </tbody>
                                                                                                                                                                            </table>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </tbody>
                                                                                                                                                            </table>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                        </div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                    <td valign='top' style='padding: 0px; min-width: 200px;' class='column' width='33.333333333333336%'>
                                                                                                                        <table align='left' cellpadding='0' cellspacing='0' width='100%' style='float: left;'>
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td valign='top' style='padding: 0px; display: table-cell;' class='innerMultiTD'><img class='imgPlaceholder' src='https://s3.amazonaws.com/imagesrepository.icommarketing.com/placeholder.gif' width='200' height='2' style='display: block; width: 200px; overflow: hidden;'></td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                                                                                    </div>
                                                                                                    <div align='center'>
                                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td align='center' style='padding: 0px;'>
                                                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='auto' align='center' style='z-index: 999;'>
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td valign='top' align='center' style='padding: 0px;'>
                                                                                                                                        <table cellpadding='0' cellspacing='0' width='100%' align='center'>
                                                                                                                                            <tbody>
                                                                                                                                                <tr>
                                                                                                                                                    <!-- Image Content -->
                                                                                                                                                    <td style='padding: 0px;'>
                                                                                                                                                        <a href='https://www.lafayettesports.com.co/' target='_blank' data-label='Pagina web Lafayette Sports'><img src='https://s3.amazonaws.com/imagesrepository.icommarketing.com/ImagesRepo/ODQ2LTE4ODQtbGFmYXlldHRlY281/1894/Pinteres/Logos/Footer-Sports.png' width='600' alt='Pagina web Lafayette Sports' border='0' style='display: block; width: 600px; height: auto; max-width: 600px; position: relative; visibility: visible;' imgsrc='' imgalt='Pagina web Lafayette Sports'></a>
                                                                                                                                                    </td>
                                                                                                                                                    <!-- End Image Content -->
                                                                                                                                                </tr>
                                                                                                                                            </tbody>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                    <div align='center' style='z-index: 999;'>
                                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td valign='top' style='padding: 0px;'>
                                                                                                                        <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td valign='top' style='padding: 0px; border-color: rgb(0, 0, 0); background: rgb(255, 255, 255);'>
                                                                                                                                        <table cellpadding='0' cellspacing='0' width='100%'>
                                                                                                                                            <tbody>
                                                                                                                                                <tr>
                                                                                                                                                    <!-- Box Content -->
                                                                                                                                                    <td style='padding: 0px; width: 100%' class='innerTD'>
                                                                                                                                                        <div align='center'>
                                                                                                                                                            <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                                                                                <tbody>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td valign='top' align='center' style='padding: 0px;'>
                                                                                                                                                                            <table cellpadding='0' cellspacing='0' border='0' width='100%'>
                                                                                                                                                                                <tbody>
                                                                                                                                                                                    <tr>
                                                                                                                                                                                        <!-- Text Content -->
                                                                                                                                                                                        <td valign='top' style='font-family: Arial; line-height: 1.2;'>
                                                                                                                                                                                            <p style='margin-top: 0px; margin-bottom: 0px; text-align: center; overflow-wrap: break-word; overflow: hidden;'><span style='color:#999999;'><em></em></span><br></p>
                                                                                                                                                                                            <p style='margin-top: 0px; margin-bottom: 0px; text-align: center; overflow-wrap: break-word; overflow: hidden;'><span style='color:#999999;'><em>Copyright © 2020 Lafayette, All rights reserved.</em></span></p>
                                                                                                                                                                                        </td><!-- End Text Content -->
                                                                                                                                                                                    </tr>
                                                                                                                                                                                </tbody>
                                                                                                                                                                            </table>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </tbody>
                                                                                                                                                            </table>
                                                                                                                                                        </div>
                                                                                                                                                    </td>
                                                                                                                                                    <!-- End Box Content -->
                                                                                                                                                </tr>
                                                                                                                                            </tbody>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>



                                                                                                    </div>
                                                                                                </td>
                                                                                                <!-- End Box Content -->
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>



                                                </div>
                                            </div>
                                        </center>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>