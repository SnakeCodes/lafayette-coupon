@component('mail::message')
# Cupón Recibido

Has recibido un cupón.



`Tu cupón es:`

```
{{$coupon->code}}
```


Recuerda que puedes usarlo en nuestra pagina de descargables

@component('mail::button', ['url' => env('WEB_URL')])
Ir a la pagina
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent