import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import ProductView from './components/ProductViewComponent';
import Home from './components/HomeComponent';
import GenerateCoupon from './components/GenerateCouponComponent';

// Routes
const router = new VueRouter({
    nase: `${process.env.APP_PUBLIC_PATH}/`,
    mode: 'history',
    linkActiveClass: 'is-active',
    routes: [
        {
            path: '',
            component: Home
        },
        {
            path: '/generate',
            component: GenerateCoupon
        },
        {
            path: '/product/:id',
            name: 'product_view',
            component: ProductView,
            props: true
        },
        {
            path: '*',
            redirect: '/404',
        },
    ],
});

export default router