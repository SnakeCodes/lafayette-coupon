<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'Api\AuthController@login');
Route::middleware('auth:api')->post('/logout', 'Api\AuthController@logout');

Route::apiResource('banners', 'Api\BannerController')->only('index');
Route::apiResource('products', 'Api\ProductController')->only(['index', 'show']);
Route::get('products/download/{product}', 'Api\ProductController@download');
Route::get('attributes/options', 'Api\AttributeProductController');

Route::group(['middleware' => 'auth:api'], function () {
    Route::apiResource('users', 'Api\UserController');
    Route::apiResource('coupons', 'Api\CouponController');
    Route::post('coupons/generate', 'Api\CouponController@generate');

    Route::apiResource('branches', 'Api\BranchController');
    Route::apiResource('campaigns', 'Api\CampaignController');
    Route::apiResource('products', 'Api\ProductController')->except(['index', 'show']);
    Route::apiResource('attributes', 'Api\AttributeController');
    Route::apiResource('clients', 'Api\ClientController');
    Route::apiResource('banners', 'Api\BannerController')->except('index');
    Route::apiResource('countries', 'Api\CountryController');
});

Route::post('coupons/check', 'Api\CouponController@check');

//Exportable
Route::get('exportable/coupons', 'Api\CouponController@export');
Route::get('exportable/clients', 'Api\ClientController@export');
Route::get('exportable/products', 'Api\ProductController@export');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
